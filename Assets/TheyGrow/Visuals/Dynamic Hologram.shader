﻿Shader "Custom/Dynamic Hologram" {
    Properties {
      _RimColor ("Rim Color", Color) = (0,0.5,0.5,0.0)
	  _CenterColor ("Center Color", Color) = (0.8,0.0,0.0,0.0)
      _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
	  _CenterPower ("Center Power", Range(0.5,8.0)) = 3.0
    }
    SubShader {
      Tags{"Queue" = "Transparent"}

      Pass {
        ZWrite On
        ColorMask 0
      }


      CGPROGRAM
      
      #pragma surface surf Lambert alpha:fade
      struct Input {
          float3 viewDir;
      };

      float4 _RimColor;
      float _RimPower;
	    float4 _CenterColor;
      float _CenterPower;
      
      void surf (Input IN, inout SurfaceOutput o) {
        half rim = saturate(.8 - saturate(dot (normalize(IN.viewDir), o.Normal)));
		    half center = saturate(dot (normalize(IN.viewDir), o.Normal)-.1);
		//   _RimColor[0] = 1.0;
          o.Emission = (_RimColor.rgb * pow (rim, _RimPower) + _CenterColor.rgb * pow (center, _CenterPower)) * 10;
          o.Alpha = pow (rim, _RimPower) + pow (center, _CenterPower);
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }