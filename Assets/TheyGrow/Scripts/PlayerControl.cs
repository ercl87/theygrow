﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Controls the firing of projectiles from the player and monitors the players position. 
/// </summary>
public class PlayerControl : MonoBehaviour {

	[SerializeField] 
	private GameObject shot1Prefab;
	[SerializeField] 
	private Text outOfBounds;
	[SerializeField] 
	private Button fireButton;
	[SerializeField]
	private float shotCooldownSeconds = 0.5f;
	// Time the player first went out of bounds.
	private float outOfBoundsStartTime = 0.0f; 
	private GameManager GMScript;
	private const float MAX_PLAYER_DIST_METERS = 10.0f;

	// Use this for initialization
	void Start () {
		GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
		GMScript = gameManager.GetComponent<GameManager>();
		// Disable the out of bounds UI text.
		outOfBounds.enabled = false;
		// Check for out of bounds periodically. 
		InvokeRepeating("CheckBounds", 1.0f, 0.25f);
	}

	/// <summary>
	/// Checks if the player is out of bounds.
	/// </summary>
	private void CheckBounds(){
		// Get the players distance from the origin of the scene.
		// When the player first starts the scene, the origin is set at their current location.
		float playerDistFromOrigin = Mathf.Abs(Vector3.Distance(transform.position, Vector3.zero));
		// Player has just gone outside of the boundary.
		if (playerDistFromOrigin > MAX_PLAYER_DIST_METERS && outOfBoundsStartTime == 0.0F ){
			// Display the out of bounds message
			outOfBounds.enabled = true;
			// Initialize the time the player first went out of bounds.
			outOfBoundsStartTime = Time.time;
		} else if (playerDistFromOrigin <= MAX_PLAYER_DIST_METERS){
			outOfBounds.enabled = false;
			outOfBoundsStartTime = 0.0f;
		}
		// The player has been out of bounds for too long. End the game.
		if ( outOfBoundsStartTime != 0.0F && Time.time - outOfBoundsStartTime > 10.0F){
			GMScript.PlayerDead ();
		// Increment the countdown on the out of bounds UI text.
		}else if (outOfBoundsStartTime != 0.0F){
			outOfBounds.text = "Out of bounds " + ((int)(11.0F - (Time.time - outOfBoundsStartTime))).ToString();
		}
	}

	/// <summary>
	/// Disables the shoot button for some time.
	/// TODO: make this generic and add it to tools.
	/// </summary>
	IEnumerator TempDisableButtonCoroutine()
    {
        fireButton.gameObject.SetActive(false);
        yield return new WaitForSeconds(shotCooldownSeconds);
        fireButton.gameObject.SetActive(true);
    }

	/// <summary>
	/// Fire the projectile.
	/// </summary>
	public void ShootProjectile(){
		// Disable the fire button for a short amount of time.
		StartCoroutine(TempDisableButtonCoroutine());
		// TODO: Pull from a pool instead of instantiating. 
		GameObject shot1 = Instantiate (shot1Prefab, Camera.main.ScreenToWorldPoint (new Vector3(0.0F,0.0F,0.0F)), Quaternion.identity);
		// Point the projectile in the right direction.
		shot1.GetComponent<Transform>().forward = transform.forward;
	}
}
