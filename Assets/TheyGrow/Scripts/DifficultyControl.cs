﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Increases the difficulty of the game based on elapsed time. 
/// </summary>
public class DifficultyControl : MonoBehaviour {
	private const float DIFFICULTY_PER_MINUTE = 1.0f; // Doubles in a minute
	private const float UPDATE_INTERVAL_SECONDS = 1.0f;
	AudioSource audioSource;
	public float Difficulty{get;private set;}

	DifficultyControl()
    {
        Difficulty = 1.0f;
    }

	void Start () {
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		audioSource = player.GetComponent<AudioSource>();
		StartCoroutine(UpdateDifficultyCoroutine());
	}

	IEnumerator UpdateDifficultyCoroutine(){
		while(true){
			// Add to the difficulty value
			Difficulty += (DIFFICULTY_PER_MINUTE / 60.0F) * UPDATE_INTERVAL_SECONDS;
			// Adjust the audio source pitch to increase as the difficulty increases. 
			audioSource.pitch = Tools.MapValue(Difficulty, 1.0F, 2.5F, 1.0F, 1.3F);
			yield return new WaitForSeconds(UPDATE_INTERVAL_SECONDS);
		}
	}

	public void ResetDifficulty(){
		Difficulty = 1.0f;
	}
}
