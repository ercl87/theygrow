﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class that controls the growth section.
/// TODO: Move a lot of the logic from the WormGrowths AddSectionSegment method into this class.
/// </summary>
public class WormGrowthSection : ProcBase {

	public MeshBuilder meshBuilder;
	// TODO: all modifications to the mesh should be done in this class once we move
	// WormGrowths AddSectionSegment method into this class.
	public Mesh mesh;

	public override Mesh BuildMesh()
	{
		meshBuilder = new MeshBuilder();
		// Create a new Mesh.
		mesh = meshBuilder.CreateMesh();
		return mesh;
	}

	/// <summary>
	/// Uses meshbuilder to update the mesh.
	/// </summary>
	public void UpdateMesh(){
		meshBuilder.UpdateMesh(mesh);
	}

	/// <summary>
	/// Moves and adjusts the collider object to fit into the section.
	/// </summary>
	public void UpdateCollider(float radius, Vector3 endPoint, Vector3 startPoint){
		GameObject colliderObj = transform.Find("Collider").gameObject;
		// Set the objects position to the middle of the section.
		colliderObj.transform.position = startPoint - (startPoint - endPoint)/2.0f;
		colliderObj.GetComponent<CapsuleCollider>().radius = radius;
		// Set the objects height to the length of the section.
		colliderObj.GetComponent<CapsuleCollider>().height = Vector3.Distance(endPoint , startPoint);
		// Face the start point.
		colliderObj.transform.LookAt(startPoint, Vector3.up);
		colliderObj.SetActive(true);

	}
}
