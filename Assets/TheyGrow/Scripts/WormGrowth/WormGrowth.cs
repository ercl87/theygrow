﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that handles the growing of the enemy. Builds upon a mesh as it grows.
/// </summary>
public class WormGrowth : MonoBehaviour {

	[SerializeField] 
	private GameObject wormGrowthSectionPrefab, headPrefab;

	[SerializeField] 
	private int radialSegmentCount = 10;

	[SerializeField] 
	private int heightSegmentCount = 10;

	[SerializeField]
	private float lengthPerGrowMeters = 0.1f;

	[SerializeField]
	private float growSpawnIntervalSeconds = 0.3f; // seconds between growths

	[SerializeField]
	private float maxFacingAngleDeviationDegrees = 40.0f;

	private GameObject playerCamera, section, head;
	private Quaternion currentRotation;
	private Quaternion prevRotation = Quaternion.identity;
	private DifficultyControl diffScript;
	private WormGrowthSection currentSection;
	
	private Vector3 prevEndPoint, currentOffset, initialSectionPoint, headingDeviation;
	private int directionLockCount = 0;
	private float prevRadius = 0.0f;
	private const int MAX_MESH_VERTICES = 55000;
	private const int MIN_DIRECTION_LOOK_CYCLES = 10;
	private const float DIRECTION_DEVIATION_CHANCE = 0.6f; 
	private const float MAX_SEGMENT_LENGTH_METERS = 0.5f;
	private const float MIN_RADIUS_METERS = 0.06f;
	private const float MAX_RADIUS_METERS = 0.08f;
	private const float RADIUS_AMPLITUDE_MODULATION_PERCENT = 0.2f;
	
	/// <summary>
	/// Growth segment transforms are always centered about the origin in order to keep the dynamic mesh
	/// reference consistent. We pass in a position on initialization so we know where to start building the 
	/// growth.
	/// </summary>
	public void Initialize (Vector3 position) {
		playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
		GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
		diffScript = gameManager.GetComponent<DifficultyControl>();

		// Initialize the starting location.
		prevEndPoint = position;
		// Create the head of the growth.
		head = (GameObject)Instantiate(headPrefab,prevEndPoint,Quaternion.identity);
		head.transform.SetParent(transform);
		// Start the first section of the growth.
		StartNewSection();
		// Start the growing process.
		StartCoroutine(GrowCoroutine());
	}

	/// <summary>
	/// Starts a new section (GameObject) of the growth. This section will be located at the end of the last one.
	/// </summary>
	private void StartNewSection(){
		// If a previous section exists, update its collider before creating the new section.
		if (section){
			section.GetComponent<WormGrowthSection>().UpdateCollider(prevRadius, prevEndPoint, initialSectionPoint);
		}
		// Record the initial start location of this section.
		initialSectionPoint = prevEndPoint;
		// Create the new section.
		section = Instantiate(wormGrowthSectionPrefab, Vector3.zero, Quaternion.identity);
		section.transform.SetParent(transform);
		
		currentSection = section.GetComponent<WormGrowthSection>();
		// Build the first ring for this section.
		currentSection.BuildRing(currentSection.meshBuilder, radialSegmentCount, prevEndPoint, prevRadius, 0, false, prevRotation);		
	}

	/// <summary>
	/// Handles the growing process. The speed of growth is controlled by the current difficulty. 
	/// </summary>
	IEnumerator GrowCoroutine(){
		while(true){
			yield return new WaitForSeconds(growSpawnIntervalSeconds/diffScript.Difficulty);
			// Update the heading Deviation.
			UpdateHeadingDeviation();
			// Get the new end point location before applying the heading deviation to it.
			Vector3 newEndPoint = prevEndPoint + Vector3.Normalize(playerCamera.transform.position - prevEndPoint)*lengthPerGrowMeters;
			// Rotate the new end point based on headingDeviation.
			newEndPoint = Tools.RotatePointAroundPivot(newEndPoint, prevEndPoint, headingDeviation);
			// Add another segment to the section.
			AddSectionSegment(newEndPoint);
		}
		
	}

	private void UpdateHeadingDeviation(){
		// Check that we have held our direction rotation for at least MIN_DIRECTION_LOOK_CYCLES growth segments.
		if (directionLockCount > MIN_DIRECTION_LOOK_CYCLES){
			directionLockCount = 0;
			// Based on DIRECTION_DEVIATION_CHANCE, apply a random deviation to the heading.
			// This deviation will be held for MIN_DIRECTION_LOOK_CYCLES growth segments.
			if (Random.Range(0.0f, 1.0f) < DIRECTION_DEVIATION_CHANCE){
				headingDeviation = new Vector3(Random.Range(-maxFacingAngleDeviationDegrees,maxFacingAngleDeviationDegrees),
									Random.Range(-maxFacingAngleDeviationDegrees,maxFacingAngleDeviationDegrees),
									Random.Range(-maxFacingAngleDeviationDegrees,maxFacingAngleDeviationDegrees));
			} else {
				// Do not deviate from target heading.
				headingDeviation = Vector3.zero;
			}	
		} 
		directionLockCount++;
	}

	/// <summary>
	/// Adds a new segment of rings to the section.
	/// TODO: Move most of this logic to the segment class.
	/// </summary>
	private void AddSectionSegment(Vector3 newEndPoint){
		
		// Check if we need to create a new segment.
		if (ShouldCreateNewSection()){
			StartNewSection();
			return;
		}
		float radius = Random.Range(MIN_RADIUS_METERS, MAX_RADIUS_METERS);
		// Build the rings for this segment.
		BuildSegmentRings(newEndPoint, radius, 0);	
		// Update the mesh with the new rings.
		currentSection.UpdateMesh();
		// Update the head location.
		UpdateHead(radius);
	}

	/// <summary>
	/// Builds the rings of the segment starting from the previous endpoint.
	/// TODO: Lerp the intermidiate end points using a Bezier curve to smooth out the elbows.
	/// </summary>
	private void BuildSegmentRings(Vector3 newEndPoint, float radius, float numModulationWaves){
		// Distances between the last end point and the new one.
		Vector3 lineDelta = (newEndPoint - prevEndPoint);
		// Calculate the final rotation state of this segment.
		Quaternion finalRotation = Quaternion.FromToRotation(Vector3.up, lineDelta.normalized);
		// Add a little randomization to the radius.
		// Build out each slice of the new segment.
		for (int i = 0; i <= heightSegmentCount; i++)
		{	
			// Calculate the percent finished we are through this segment.
			float finishedPercent = (float)i/heightSegmentCount;
			// Calculate theta for the radius modulation.
			float theta = (finishedPercent)*2.0F*Mathf.PI;
			// currentOffset += lineDelta.normalized * (float)(heightInc * i);
			// Calculate the offset for this slice.
			currentOffset = prevEndPoint + lineDelta*(finishedPercent);
			// Smooth out the transition from the previous radius and the new one.
			float curRadius = Mathf.Lerp(prevRadius, radius, Mathf.Exp(finishedPercent) - 1.0f);
			// Modulate the radius.
			curRadius +=  RADIUS_AMPLITUDE_MODULATION_PERCENT * curRadius * Mathf.Sin(numModulationWaves * theta);
			// Smooth out the rotation from the previous rotation and the new one.
			currentRotation = Quaternion.Lerp(prevRotation, finalRotation, (i+1)/heightSegmentCount);
			// Build the ring for this slice.
			currentSection.BuildRing(currentSection.meshBuilder, radialSegmentCount, currentOffset, curRadius, finishedPercent, true, currentRotation);
		}
		// Update the previous values to the current.
		prevRadius = radius;
		prevEndPoint = newEndPoint;
		prevRotation = currentRotation;
	}

	/// <summary>
	/// Check if we need to create a new section. 
	/// </summary>
	private bool ShouldCreateNewSection(){
		// 65k vertices is the limit.
		// If our segment is longer than MAX_SEGMENT_LENGTH_METERS, then lets start a new one.
		if (currentSection.mesh.vertexCount > MAX_MESH_VERTICES || Vector3.Distance(prevEndPoint, initialSectionPoint) > MAX_SEGMENT_LENGTH_METERS){
			return true;
		}
		return false;
	}

	/// <summary>
	/// Updates the location, rotation, and scale of the leading head.
	/// </summary>
	private void UpdateHead(float radius){
		head.transform.position = prevEndPoint;
		head.transform.LookAt(playerCamera.transform.position);
		head.transform.localScale = new Vector3(2.2f, 2.2f, 1.0f)*radius;
	}
}
