﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EffectCollision : MonoBehaviour {

	public GameObject explosion;

	void OnCollisionEnter(Collision collision)
	{	
		if (collision.transform.root.tag == "Growth"){
			Instantiate (explosion, transform.position, Quaternion.identity);
			Destroy (gameObject);
			
		}
	}
}
