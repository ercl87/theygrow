﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Class for displaying the direction if the closest enemy on the UI.
/// TODO: Needs to be optimized. Currently grabing all objects with tag. This grows into massive list
/// that we then calculate the distance to each object. Recommend breaking the world space into quadrants 
/// and then doing a proximity search.
/// </summary>
public class Proximity : MonoBehaviour {

	private GameObject playerCamera;
	private Vector3 playerPosition;
	private GameObject gameManager;
	private GameObject[] enemies;
	private GameManager GMScript;

	public GameObject proximityMarker;
	private const int NUMBER_NEARBY_ENEMY_MARKERS = 1;
	private const float UPDATE_ENEMY_SEARCH_INTERVAL_SECONDS = 0.5f;
	private const float UPDATE_PAINT_INTERVAL_SECONDS = 0.05f;

	void Start () {
		gameManager = GameObject.FindGameObjectWithTag("GameManager");
		GMScript = gameManager.GetComponent<GameManager>();
		playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
		proximityMarker.SetActive(false);
		StartCoroutine(UpdateEnemyProximitiesCoroutine());
		StartCoroutine(PaintCoroutine());
	}

	/// <summary>
	/// Returns a list of enemies sorted by proximity to playerCamera.
	/// </summary>
	public GameObject[] FindNClosestEnemies(int numberEnemies){
		return GameObject.FindGameObjectsWithTag("GrowthSection")
			.OrderBy(enemy => Vector3.Distance(enemy.transform.position, playerCamera.transform.position))
			.Take(numberEnemies)
			.ToArray();
    }

	IEnumerator UpdateEnemyProximitiesCoroutine(){
		while(true){
			enemies = FindNClosestEnemies(NUMBER_NEARBY_ENEMY_MARKERS);
			yield return new WaitForSeconds(UPDATE_ENEMY_SEARCH_INTERVAL_SECONDS);
		}
		
	}

	/// <summary>
	/// Move the proximity indicator.
	/// </summary>
	IEnumerator PaintCoroutine() {
		while(true){
			yield return new WaitForSeconds(UPDATE_PAINT_INTERVAL_SECONDS);

			if (enemies.Count() <1 || enemies[0] == null){
				continue;
			}
			GameObject obj = enemies[0];
			// Quaternion rotation = Quaternion.Euler(0,0, 0);
			// GameObject marker = Instantiate(proximityMarker, obj.transform.position, rotation);
			Vector3 screenpos = Camera.main.WorldToScreenPoint(obj.transform.position);
			
			if (screenpos.z > 0 &&
				screenpos.x > 0 && 
				screenpos.x < Screen.width &&
				screenpos.y > 0 &&
				screenpos.y < Screen.height) {
				proximityMarker.SetActive(false);
			} else {
				if (screenpos.z < 0){
					// Flip arrows when enemy is behind playerCamera
					screenpos *= -1;
				}

				Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0)/2;

				// make 00 the center of screen instead of bottom left
				screenpos-= screenCenter;

				// find angle from center of screen
				float angle = Mathf.Atan2(screenpos.y, screenpos.x);
				// angle -= 90 * Mathf.Deg2Rad;

				float cos = Mathf.Cos(angle);
				float sin = Mathf.Sin(angle);

				screenpos = screenCenter + new Vector3(sin * 150, cos * 150, 0);

				// float m = cos / sin; // y = mx + b
				float m = sin / cos;

				Vector3 screenBounds = screenCenter * 0.9f;
				
				// check up and down first
				if (sin > 0) {
					screenpos = new Vector3(screenBounds.y / m, screenBounds.y, 0);
				} else {
					screenpos = new Vector3(-screenBounds.y / m, -screenBounds.y, 0);
				}

				// if out of bounds, get point on correct side
				if (screenpos.x > screenBounds.x) { // out of bounds right
					screenpos = new Vector3(screenBounds.x, screenBounds.x*m, 0);
				} else if (screenpos.x < -screenBounds.x) { // out of bounds left
					screenpos = new Vector3(-screenBounds.x, -screenBounds.x*m, 0);
				} //else :in bounds

				// remove coordinate translation
				screenpos += screenCenter;

				Quaternion rotation = Quaternion.Euler(0,0, ((-90 * Mathf.Deg2Rad) + angle) * Mathf.Rad2Deg);
				proximityMarker.transform.SetPositionAndRotation(screenpos, rotation);
				proximityMarker.SetActive(true);				
			}
		}
	}
}
