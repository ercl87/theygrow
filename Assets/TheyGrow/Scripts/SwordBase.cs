﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Class that controls the sword base. It will try to smoothly follow the target object;
/// </summary>
public class SwordBase : MonoBehaviour {

	// Target to follow and alight rotation to.
	[SerializeField] 
	private GameObject swordTarget;
	[SerializeField]
	private Image target;
	private GameObject playerCamera;
	private const float MOVING_AVERAGE_MULT = 0.33f;
	private const float ROTATIONAL_AVERAGE_MULT = 0.125f;
	
	// Use this for initialization
	void Start () {
		playerCamera = GameObject.FindGameObjectWithTag("MainCamera");
		// Initialize the moving average position and rotation.
		transform.position = swordTarget.transform.position;
		transform.rotation = swordTarget.transform.rotation;
	}
	
	void FixedUpdate () {
		UpdateLoc();
		UpdateRot();
	}

	private void UpdateLoc(){
		transform.position = Vector3.Lerp(transform.position, swordTarget.transform.position, MOVING_AVERAGE_MULT);
		transform.position = transform.position;
	}

	private void UpdateRot(){
		transform.rotation = Quaternion.Lerp(transform.rotation, swordTarget.transform.rotation, ROTATIONAL_AVERAGE_MULT);
		transform.rotation = transform.rotation;
	}

	public void ToggleSword(){
		target.gameObject.SetActive(gameObject.activeSelf);
		gameObject.SetActive(!gameObject.activeSelf);
	}

}
