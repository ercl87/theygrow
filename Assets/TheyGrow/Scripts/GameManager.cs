﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


#if UNITY_ADS
using UnityEngine.Advertisements;
#endif
/// <summary>
/// Manages the gameflow, UI and ads. 
/// </summary>
public class GameManager : MonoBehaviour {

	[SerializeField]
	private GameObject scoreObj, maxScoreObj, gameOverMessageObj, restartGameButtonObj, mainMenuButtonObj;

	private DifficultyControl diffScript;

	private AudioSource audioSource;
	private Text scoreText, maxScoreText;
	private int score = 0;
	private int highscore;
	private const float SCORE_UPDATE_INTERVAL_SECONDS = 0.25f; // seconds between updates
	private const int PLAYS_BETWEEN_ADS = 2;
	
	void Start () {
		diffScript = gameObject.GetComponent<DifficultyControl>();
		GameObject player = GameObject.FindGameObjectWithTag("Player");
		audioSource = player.GetComponent<AudioSource>();

		// Hide game over UI elements
		SetGameOverMsgVisibility(false);
		InitializeScore();
		InitializeGame();
	}
	/// <summary>
	/// Enables/Disables the game over UI Elements
	/// </summary>
	private void SetGameOverMsgVisibility(bool disp){
		gameOverMessageObj.SetActive(disp);
		restartGameButtonObj.gameObject.SetActive(disp);
		mainMenuButtonObj.gameObject.SetActive(disp);
	}

	private void InitializeScore(){
		scoreText = scoreObj.GetComponent<Text>();
		maxScoreText = maxScoreObj.GetComponent<Text>();
		highscore = PlayerPrefs.GetInt ("highscore", 0);
	}
	
	/// <summary>
	/// Initializes the game. Should be called on Scene startup.
	/// </summary>
	private void InitializeGame () {
		// Disable the music
		audioSource.enabled = false;
		// Freeze the scene until we have finished the initialization.
		Time.timeScale = 0.0f;	
		/// Disabled for now due to Unity Ads buggy when sharing the project	
		// if (!TryShowAds()){
		// 	StartGame();
		// }
		StartGame();
	}

	/// <summary>
	/// Starts the gameplay.
	/// </summary>
	public void StartGame(){
		// Disable game over UI elements.
		SetGameOverMsgVisibility(false);
		// Reset any enemies that may still be in the scene.
		ResetEnemies();
		// Reset the current score UI text.
		score = 0;
		scoreText.text = "Score: 0";
		// Reset the game Difficulty.
		diffScript.ResetDifficulty();
		// Start the game by setting the timescale to 1.0f;
		Time.timeScale = 1.0f;
		// Start the music!
		audioSource.enabled = true;
	}
	
	/// <summary>
	/// Attempts to show ads to the player. If the ad is shown, the game will be started by a callback 
	/// once the ad has finished. Returns false if no ad was shown.
	/// </summary>
	private bool TryShowAds(){
		#if UNITY_ADS
		// Check how many times the player has played since the last ad. If it is equal to PLAYS_BETWEEN_ADS,
		// then show them an ad.
		int countToAd = PlayerPrefs.GetInt("count_to_ad", 0);
		if (PlayerPrefs.GetInt("ads_disabled", 0) == 0 && countToAd >= PLAYS_BETWEEN_ADS){
			PlayerPrefs.SetInt("count_to_ad",PLAYS_BETWEEN_ADS);
			if (!Advertisement.IsReady())
			{
				Debug.Log("Ads not ready for default placement");
				return false;
			}
			// Disabled for now due to Unity Ads buggy when sharing the project
			// ShowOptions options = new ShowOptions();
			// The HandleShowResult callback will start the game after the ad has finished.
			// options.resultCallback = HandleShowResult;
			// Advertisement.Show(options);
			return true;
        	
		}else{
			PlayerPrefs.SetInt("count_to_ad",countToAd + 1);
			return false;
		}
		#endif
		return false;
	}

	/// <summary>
	/// Callback called when the user finishes watching the ad.
	/// Disabled due to Unity Ads buggy when sharing the project
	/// </summary>
	/// <param name="result"></param>
	// private void HandleShowResult (ShowResult result)
	// {
	// 	// Set the count to ad back to 0.
	// 	PlayerPrefs.SetInt("count_to_ad",0);
	// 	StartGame();
	// 	if(result == ShowResult.Finished) {
	// 		Debug.Log("Video completed - Offer a reward to the player");

	// 	}else if(result == ShowResult.Skipped) {
	// 		Debug.LogWarning("Video was skipped - Do NOT reward the player");

	// 	}else if(result == ShowResult.Failed) {
	// 		Debug.LogError("Video failed to show");
	// 	}
	// }

	private void UpdateHighScore(){
		if (score > highscore) {
			PlayerPrefs.SetInt("highscore", score);
			maxScoreText.text = "Max Score: " + score;
		}
	}

	public void IncrementScore(){
		score += 1;
		scoreText.text = "Score: " + score;
	}

	/// <summary>
	/// Destroys all enemies in the scene.
	/// </summary>
	private void ResetEnemies(){
		foreach(GameObject enemy in GameObject.FindGameObjectsWithTag("Growth"))
		{
			GameObject.Destroy(enemy);
		}
	}

	public void LoadMainMenu(){
		SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
	}

	/// <summary>
	/// Handles the end of game death sequence.
	/// </summary>
	public void PlayerDead(){
		/*
		The player is dead. lets freeze time for now. 
		TODO: add location of impact of the enemy so the player can see how they were hit.
		 */
		// Turn off the music.
		audioSource.enabled = false;
		// Freeze the scene.
		Time.timeScale = 0.0f;
		// Show the game over UI elements.
		SetGameOverMsgVisibility(true);
		// Update the high score.
		UpdateHighScore();
	}
}