﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that handles the spawning of enemies. This class does not utilize the AR point cloud 
/// for spawn locations.
/// </summary>
public class SpawnerNoAR : MonoBehaviour {

	[SerializeField]
	private GameObject growth;
	[SerializeField] 
	private GameObject player;
	private GameManager GMScript;
	private DifficultyControl DiffScript;

	private const float MIN_SPAWN_DIST_METERS = 2.0f;
	private const float MAX_SPAWN_DIST_METERS = 5.0f;
	private const float MAX_VERTICAL_SPAWN_DIST_METERS = 2f;
	private const float SPAWN_INTERVAL_SECONDS = 2.0f;

	void Start () {
		// Find the GameManager in order to interface with it
		GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
		GMScript = gameManager.GetComponent<GameManager>();

		DiffScript = gameManager.GetComponent<DifficultyControl>();
		StartCoroutine(HandleGrowthsCoroutine());
	}

	/// <summary>
	/// Spawns enemies in random locations.
	/// TODO: (Critical for performance) Add a maximum number of enemies that can be active.
	/// </summary>
	IEnumerator HandleGrowthsCoroutine(){
		while(true){
			// Spawn speed is based on the current game difficulty.
			yield return new WaitForSeconds(SPAWN_INTERVAL_SECONDS/DiffScript.Difficulty);
			SpawnNewGrowth();
		}
	}

	/// <summary>
	/// Creates a new growth at a random location.
	/// </summary>
	private void SpawnNewGrowth(){
		// Instantiate a new growth at this spawn location.
		GameObject newGrowth = (GameObject)Instantiate (growth, Vector3.zero, Quaternion.identity);
		// Initialize the new growth by telling it where to start growing from.
		newGrowth.GetComponent<WormGrowth>().Initialize(GetSpawnLocation());
	}

	/// <summary>
	/// Calculates a random spawn location around the player.
	/// </summary>
	private Vector3 GetSpawnLocation(){
		// Get a random spawn distance from the player.
		float spawnDist = Random.Range (MIN_SPAWN_DIST_METERS, MAX_SPAWN_DIST_METERS);
		// We dont want the enemies spawning to high above the player, so we 
		// get a random vertical offset that is within reason.
		// TODO: make this based on vertical angle for a more predictable outcome.
		float vertSpawnDist = Random.Range(-MAX_VERTICAL_SPAWN_DIST_METERS,MAX_VERTICAL_SPAWN_DIST_METERS);
		// Calculate the x rotation (about the player) for the spawn point.
		float xRotation = Mathf.Sin(vertSpawnDist/spawnDist) * Mathf.Rad2Deg;
		// Calculate the y rotation (about the player) for the spawn point.
		float yRotation = Random.Range (0.0F, 360.0F);

		// Create a rotations vector.
		Vector3 rotations = new Vector3(xRotation,yRotation,0.0F);
		// Set the initial spawn point about the x,y plane at set spawnDist.
		Vector3 initialSpawnPoint = player.transform.position + new Vector3(0,0,spawnDist);
		// Rotate the spawn point about the player by the rotations vector.
		return Tools.RotatePointAroundPivot(initialSpawnPoint, player.transform.position, rotations);
	}
	
}
