﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Detects collisions between the player and enemies.
/// </summary>
public class Player : MonoBehaviour {
	GameManager GMScript;

	// Use this for initialization
	void Start () {
		GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
		GMScript = gameManager.GetComponent<GameManager>();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.transform.root.tag == "Growth"){
			// Tell the game manager the player has died.
			GMScript.PlayerDead ();
		}
	}
}
