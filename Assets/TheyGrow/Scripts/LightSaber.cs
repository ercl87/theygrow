﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Check for collision with enemies.
/// </summary>
public class LightSaber : MonoBehaviour {

	[SerializeField] private GameObject explosion;
	private GameManager GMScript;

	// Use this for initialization
	void Start () {
		GameObject gameManager = GameObject.FindGameObjectWithTag("GameManager");
		GMScript = gameManager.GetComponent<GameManager>();
	}

	void OnTriggerEnter(Collider collision)
	{
		if (collision.transform.root.tag == "Growth"){
			// Create an explosion at impact location. 
			Instantiate (explosion, collision.transform.position, Quaternion.identity);
			// Increment the score.
			GMScript.IncrementScore();
			// Destroy the root gameobject.
            Destroy(collision.transform.root.gameObject);
		}
	}
}
