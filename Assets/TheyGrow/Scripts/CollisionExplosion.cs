﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Destroyes Enemies withing radius for a short period of time.
/// </summary>
public class CollisionExplosion : MonoBehaviour {
    [SerializeField]
	private float radius;// explosion radius

    private GameObject gameManager;
    private GameManager GMScript;

    // Hash set used to store the ID of killed GameObjects.
    private HashSet<int> killedGrowths = new HashSet<int>();
	public void Start(){
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
        GMScript = gameManager.GetComponent<GameManager>();
        DestroyCloseEnemies();
    }

    private void DestroyCloseEnemies() {
        // Find all colliders within radius
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);// create explosion
        for(int i=0; i<hitColliders.Length; i++)
        {   
            // Check that this is a Growth and that we have not already killed the colliders parent object.
            if(hitColliders[i].transform.root.tag == "Growth" &&
                  !killedGrowths.Contains(hitColliders[i].transform.root.GetInstanceID()))
            {   
                // Add the objects ID to avoid killing it again.
                killedGrowths.Add(hitColliders[i].transform.root.GetInstanceID());
                GMScript.IncrementScore();
                // Destroy the gameobject
                Destroy(hitColliders[i].transform.root.gameObject);
            }
			
        }
        // TODO: Should have the life of this object based on a time passed to it.
        Destroy(gameObject, 1.0f);// destroy explosion
	}
}
 
    