﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;
using UnityEngine.SceneManagement;

/// <summary>
/// Checks your device and OS to make sure it supports ARkit. If not supported, it will 
/// load a warning message.
/// </summary>
public class SupportCheck : MonoBehaviour {

    const float MINIMUM_SUPPORTED_IOS_VERSION = 11f;
	enum SUPPORT_TYPE_ {SUPPORTED, BAD_VERSION, OLD_DEVICE, UNKNOWN};
    private HashSet<DeviceGeneration> UNSUPPORTED_OLD_IOS_DEVICES = 
            new HashSet<DeviceGeneration>(){
        {DeviceGeneration.iPhone4},
        {DeviceGeneration.iPhone4S},
        {DeviceGeneration.iPhone5},
        {DeviceGeneration.iPhone5C},
        {DeviceGeneration.iPhone5S},
        {DeviceGeneration.iPhone6},
        {DeviceGeneration.iPhone6Plus},
        {DeviceGeneration.iPad1Gen},
        {DeviceGeneration.iPad2Gen},
        {DeviceGeneration.iPad3Gen},
        {DeviceGeneration.iPad4Gen},
        {DeviceGeneration.iPadAir1},
        {DeviceGeneration.iPadAir2},
        {DeviceGeneration.iPadMini1Gen},
        {DeviceGeneration.iPadMini2Gen},
        {DeviceGeneration.iPadMini3Gen},
        {DeviceGeneration.iPadMini4Gen},
        {DeviceGeneration.iPodTouch1Gen},
        {DeviceGeneration.iPodTouch2Gen},
        {DeviceGeneration.iPodTouch3Gen},
        {DeviceGeneration.iPodTouch4Gen},
        {DeviceGeneration.iPodTouch5Gen},
        {DeviceGeneration.iPodTouch6Gen}
    };

    void Start () {
		SUPPORT_TYPE_ supported = IsSupportedArKit();
        switch (supported) {
            case SUPPORT_TYPE_.BAD_VERSION:
                SceneManager.LoadScene("DeviceVersionOld", LoadSceneMode.Single);
                break;
            case SUPPORT_TYPE_.OLD_DEVICE:
                SceneManager.LoadScene("DeviceNotSupported", LoadSceneMode.Single);
                break;
        }
		enabled = false;
	}

	private SUPPORT_TYPE_ IsSupportedArKit()
    {
        #if UNITY_EDITOR
        return SUPPORT_TYPE_.SUPPORTED;
        #endif
 
        #if UNITY_IOS
        var iOSGeneration = Device.generation;
        Debug.Log("iOSGeneration:" + iOSGeneration);

        if (UNSUPPORTED_OLD_IOS_DEVICES.Contains(iOSGeneration)) {
            Debug.Log("Device not supported");
            return SUPPORT_TYPE_.OLD_DEVICE;
        }

		Debug.Log("UnityEngine.iOS.Device.systemVersion:" + Device.systemVersion);
        float iOSVersion = MINIMUM_SUPPORTED_IOS_VERSION;
        string[] version = Device.systemVersion.Split('.');
        float.TryParse(version[0], out iOSVersion);
        if (iOSVersion < MINIMUM_SUPPORTED_IOS_VERSION)
        {
            Debug.Log("Not supported iOS version: " + iOSVersion);
            return SUPPORT_TYPE_.BAD_VERSION;
        }
 
        return SUPPORT_TYPE_.SUPPORTED;
        #endif
    }
}
