﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	[SerializeField] private Text highScore;
	[SerializeField] private Button disableAdsButton;

	// Use this for initialization
	void Start () {
		UpdateHighScore();
		MaybeDisableAds();
	}

	private void UpdateHighScore(){
		highScore.text = "Max Score: " + PlayerPrefs.GetInt("highscore", 0);
	}

	private void MaybeDisableAds(){
		disableAdsButton.gameObject.SetActive(PlayerPrefs.GetInt("ads_disabled", 0) == 0);
	}

	public void PlayGame(){
		SceneManager.LoadScene("Level1", LoadSceneMode.Single);
	}

}
