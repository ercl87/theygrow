﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Tools{

	/// <summary>
	/// Maps a value from one range to another;
	/// </summary>
	/// <param name="value"> Value to be mapped.</param>
	/// <param name="a1"> Lower bound of original range.</param>
	/// <param name="a2"> Upper bound of original range.</param>
	/// <param name="b1"> Lower bound of new range.</param>
	/// <param name="b2"> Upper bound of new range</param>
	public static float MapValue(float value, float a1, float a2, float b1, float b2)
	{
		float val = b1 + (value-a1)*(b2-b1)/(a2-a1);
		val = Mathf.Min(val, b2);
		val = Mathf.Max(val, b1);
		return val;
	}

	/// <summary>
	/// Applies a rotation of the given point about the pivot by the angles vector.
	/// </summary>
	/// <param name="point"> Point to be rotated.</param>
	/// <param name="pivot"> Point at which to rotate about.</param>
	/// <param name="angles"> Vector3 of rotations.</param>
	public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
    	return Quaternion.Euler(angles) * (point - pivot) + pivot;
  	}

}

