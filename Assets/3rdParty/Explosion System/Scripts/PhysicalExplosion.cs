using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class PhysicalExplosion : MonoBehaviour 
{
    public float Radius;// explosion radius
    public float Force;// explosion forse

    private GameObject GameManager = null;
    GameManager GMScript;
    private HashSet<int> killedGrowths;

    public void Start(){
        GameManager = GameObject.Find ("GameManager");
        GMScript = GameManager.GetComponent<GameManager>();
    }

     // TODO: This should not be done in Update. Move to a collider
    void Update () 
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, Radius);// create explosion
        for(int i=0; i<hitColliders.Length; i++)
        {              
            if(hitColliders[i].transform.root.tag == "Growth" &&
                  !killedGrowths.Contains(hitColliders[i].transform.root.GetInstanceID()))
            {
                killedGrowths.Add(hitColliders[i].transform.root.GetInstanceID());
                GMScript.IncrementScore();
                Destroy(hitColliders[i].transform.root.gameObject);
            }
			
        }
        Destroy(gameObject,0.2f);// destroy explosion
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,Radius);
    }
}