# They Grow #

This is a very simple Augmented Reality app where the user must destroy the enemies before getting overwhelmed. 

Your tools are a sword, gun and your movement.  

# Supported Devices #

All apple ios devices that support ARkit.

Requires ios 11 or greater.
